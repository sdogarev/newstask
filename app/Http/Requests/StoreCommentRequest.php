<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                 => 'required|min:2|max:256|string|regex:/[a-zA-Z0-9]+/',
            'email'                => 'required|min:2|max:256|string|email',
            'text'                 => 'required|min:2|max:5000|regex:/[а-яА-Яa-zA-Z0-9]+/|not_regex:/<[а-яА-Яa-zA-Z0-9]*>/',
            'article_id'           => 'required|numeric|not_regex:/[a-z]/|exists:App\Models\Article,id',
            'g-recaptcha-response' => 'required',
        ];
    }
}
