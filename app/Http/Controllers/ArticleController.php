<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    /**
     * Получить все опубликованые новости
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('articles.index', [
            'articles' => Article::where('published', 1)
                ->orderBy('created_at', 'desc')
                ->orderBy('views', 'desc')
                ->paginate(3)
        ]);
    }

    /**
     * Страница с новостью
     * @param $id - новости
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show($id)
    {
        $article = Article::findOrFail($id);

        $comments = $article->comments()
            ->where('published', 1)
            ->orderBy('created_at', 'asc')
            ->paginate(1);

        // Если в данной сессии мы уже смотрели эту запись, то не добавлять больше просмотры
        if(!session("article_$article->id")) {
            $article->update(['views' => ++$article->views]);
            session(["article_$article->id" => 'viewed']);
        }

        return view('articles.show', [
            'article'  => $article,
            'comments' => $comments
        ]);
    }
}
