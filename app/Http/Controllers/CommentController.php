<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;
use App\Http\Requests\StoreCommentRequest;

class CommentController extends Controller
{
    /**
     * Добавление комментария на странице
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreCommentRequest $request)
    {
        Comment::create($request->all());

        return redirect()->back()->with('message', 'Комментарий успешно создана. Пожалуйста подождите пока модерация одобрит вашу запись');
    }
}
