<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    /**
     * Получить страницу в Админке со всеми отзывами
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('admin.comments.index', ['comments' => Comment::orderBy('id', 'desc')->get()]);
    }

    /**
     * Получить страницу в Админке с отзывом
     * @param $id - отзыва
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function show($id)
    {
        return view('admin.comments.show', ['comment' => Comment::findOrFail($id)]);
    }

    /**
     * Получить страницу с редактированием отзыва
     * @param $id - отзыва
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        return view('admin.comments.edit', ['comment' => Comment::findOrFail($id)]);
    }

    /**
     * Обновление текста отзыва или обновление статуса
     * @param $id - отзыва
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, Request $request)
    {
        // Если есть text то мы меняем комментарий
       if($request->text) {
           $request->validate(['text' => 'required|min:2|max:5000|regex:/[а-яА-Яa-zA-Z0-9]+/',]);

           $comment = Comment::find($id);
           $comment->update($request->all());

           return redirect()->route('admin.comments.index')->with('message', 'Комментарий успешно изменён');
       }
        // Если текста не было, значит это была публикация
        $published = Comment::find($id);
        $published->update(['published' => 1]);

        return redirect()->route('admin.comments.index')->with('message', 'Комментарий успешно опубликован');
    }

    /**
     * Удаление отзыва
     * @param $id - отзыва
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        Comment::destroy($id);

        return redirect()->route('admin.comments.index')->with('message', 'Комментарий успешно удален');
    }
}
