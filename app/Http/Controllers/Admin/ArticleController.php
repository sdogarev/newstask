<?php

namespace App\Http\Controllers\Admin;

use App\Events\ArticleEvent;
use App\Http\Controllers\Controller;
use App\Models\Article;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\StoreArticleRequest;

class ArticleController extends Controller
{
    /**
     * Получение всех опубликованых и не опубликованых новостей
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('admin.articles.index', [
            'articlesAll' => Article::orderBy('published', 'asc')->get()
        ]);
    }

    /**
     * Страница создания новой новости
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create()
    {
        return view('admin.articles.create');
    }

    /**
     * Создание новой новости
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(StoreArticleRequest $request)
    {
        $path = $request->file('image') !== null ? $request->file('image')->store('articles') : null;
        $params = $request->all();
        $params['image'] = $path;

        Article::create($params);

        return redirect()->route('admin.articles.index')->with('message', 'Публикация успешно создана');
    }

    /**
     * Обновление статуса новости (Задачи не было обновлять новость!? )
     * @param $id - новости
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, Request $request)
    {
        $article = Article::findOrFail($id);
        if($request->published == 1) {
            $article->update([
                'published' => $request->published
            ]);
        } elseif($request->published == 0) {
            $article->update([
                'published' => $request->published
            ]);
        } else {
            return redirect()->route('admin.articles.index')->with('message', 'Ошибка параметра');
        }
        

        return redirect()->route('admin.articles.index')->with('message', 'Публикация успешно опубликована');
    }

    /**
     * Удаление новости
     * @param $id - новости
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $image = Article::find($id)->image;

        if($image) {
            event(new ArticleEvent($image));
        };
        
        Article::destroy($id);

        return redirect()->route('admin.articles.index')->with('message', 'Публикация успешно удалена');
    }
}
