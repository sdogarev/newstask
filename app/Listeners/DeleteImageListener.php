<?php

namespace App\Listeners;

use App\Events\ArticleEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Storage;

class DeleteImageListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ArticleEvent  $event
     * @return void
     */
    public function handle(ArticleEvent $event)
    {
        if($event->image !== null) {
            Storage::delete($event->image);
        }
    }
}
