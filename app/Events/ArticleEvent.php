<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class ArticleEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $image;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($image)
    {
        $this->image = $image;
    }
}
