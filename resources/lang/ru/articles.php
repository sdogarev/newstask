<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Языковые ресурсы абонементов
    |--------------------------------------------------------------------------
    |
    | Последующие языковые строки возвращают ошибки, текст, заголовки и т.д
    | связанные с Тестовыми абонементами
    |
    |
    */

    'articles_index_link_readmore' => 'Читать дальше ->',
    'articles_index_created_at' => 'Создано',
    'articles_index_views' => 'Просмотров',
    'articles_admin_index_btn_add_news' => 'Добавить новость',
    'articles_admin_index_btn_publish' => 'Опубликовать',
    'articles_admin_index_btn_delete' => 'Удалить',

    'articles_admin_create_1' => 'Заголовок',
    'articles_admin_create_2' => 'Текст',
    'articles_admin_create_3' => 'Картинка',
];
