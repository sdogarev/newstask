@extends('layouts.app')

@section('content')
    @if(session('message'))
        <div class="alert alert-success" role="alert">
            {{ session('message') }}
        </div>
    @endif
    @foreach($comments as $comment)
        <div class="container">
            <div class="card mb-3">
                <div class="card-body">
                    <h5 class="card-title">Name: {{ $comment->name }}</h5>
                    <p class="card-text">{{ mb_substr($comment->text, 0, 150) }}...</p>
                    <p class="card-text"><a class="btn btn-link" href="{{ route('admin.comments.show', $comment->id) }}">Просмотреть полностью отзыв</a></p>
                    <p class="card-text"><small class="text-muted">{{ __('articles.articles_index_created_at') }}: {{ $comment->created_at }} | {{ $comment->published ? 'Опубликована' : 'Не опубликована' }}</small></p>
                </div>
            </div>
        </div>
    @endforeach

@endsection
