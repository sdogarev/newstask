@extends('layouts.app')

@section('content')
@include('admin.comments.partial.form', ['comment' => $comment])
@endsection
