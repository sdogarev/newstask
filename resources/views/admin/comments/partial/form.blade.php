<div class="container">
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger" role="alert">
                {{ $error }}
            </div>
        @endforeach
    @endif
    <form action="{{ route('admin.comments.update', $comment->id) }}" method="POST" class="mb-5">
        @csrf
        @method('PUT')
        <label for="exampleInputEmail1" class="form-label">Имя</label>
        <input type="text" class="form-control" name="name" value="{{ $comment->name }}" disabled="disabled" id="exampleInputEmail1" aria-describedby="emailHelp">

        <label for="exampleInputEmail1" class="form-label">E-майл</label>
        <input type="email" class="form-control" name="email" value="{{ $comment->email }}" disabled="disabled" id="exampleInputEmail1" aria-describedby="emailHelp">

        <label for="exampleInputEmail1" class="form-label">Текст</label>

        <textarea class="form-control" name="text" id="exampleFormControlTextarea1" rows="3">{{ $comment->text }}</textarea>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>

</div>
