@extends('layouts.app')

@section('content')
    <div class="container">
        @if(session('message'))
            <div class="alert alert-success" role="alert">
                {{ session('message') }}
            </div>
        @endif
        <div class="card mb-3">
            <div class="card-body">
                <h5 class="card-title">{{ $comment->name }}</h5>
                <p class="card-text">{{ $comment->text }}</p>
                <p class="card-text"><small class="text-muted">{{ __('articles.articles_index_created_at') }}: {{ $comment->created_at }} | В новости:
                        <a href="{{ route('articles.show', $comment->article->id) }}">{{ $comment->article->title }}</a></small></p>
                @if(!$comment->published)
                    <form action="{{ route('admin.comments.update', $comment->id) }}" method="POST" style="display: inline-block">
                        @csrf
                        @method('PUT')
                        <button class="btn btn-primary mb-5">Опубликовать</button>
                    </form>
                @endif
                <a class="btn btn-info mb-5" href="{{ route('admin.comments.edit', $comment->id) }}">Редактировать</a>
                <form action="{{ route('admin.comments.destroy', $comment->id) }}" method="POST" style="display: inline-block">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger mb-5">Удалить</button>
                </form>
            </div>
        </div>
    </div>
@endsection
