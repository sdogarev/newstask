@extends('layouts.app')

@section('content')
    <div class="container">
        @if(session('message'))
            <div class="alert alert-success" role="alert">
                {{ session('message') }}
            </div>
        @endif
        <a class="btn btn-primary mb-5" href="{{ route('admin.articles.create') }}">{{ __('articles.articles_admin_index_btn_add_news') }}</a>
        <a class="btn btn-primary mb-5" href="{{ route('admin.comments.index') }}">Гостевая книга</a>
        <div class="row">
            <h1>Все новости</h1>
            <hr>
        </div>
        <div class="row">
            @foreach($articlesAll as $article)
                <div class="card mb-4 col-md-4">
                    <strong>{{ $article->published ? 'Опубликована' : 'Не опубликована' }}</strong>
                    <img src="{{ Storage::url($article->image) }}" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title"><a href="{{ route('articles.show', $article->id) }}">{{ $article->title }}</a></h5>
                        <p class="card-text">{{ $article->text }}</p>
                        <p class="card-text"><a class="btn btn-link" href="{{ route('articles.show', $article->id) }}">{{ __('articles.articles_index_link_readmore') }}</a></p>
                        <p class="card-text"><small class="text-muted">{{ __('articles.articles_index_created_at') }}: {{ $article->updated_at }} | {{ __('articles.articles_index_views') }}: {{ $article->views }}</small></p>
                        <div class="d-flex">
                            @if(!$article->published)
                            <form class="mr-2" action="{{ route('admin.articles.update', $article->id) }}" method="POST">
                                @csrf
                                @method('PUT')
                                <input type="hidden" name="published" value="1">
                                <button type="submit" class="btn btn-primary mb-5">{{ __('articles.articles_admin_index_btn_publish') }}</button>
                            </form>
                            @else
                            <form class="mr-2" action="{{ route('admin.articles.update', $article->id) }}" method="POST">
                                @csrf
                                @method('PUT')
                                <input type="hidden" name="published" value="0">
                                <button type="submit" class="btn btn-danger mb-5">снять с публикации</button>
                            </form>
                            @endif
                            <form action="{{ route('admin.articles.destroy', $article->id) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-primary mb-5">{{ __('articles.articles_admin_index_btn_delete') }}</button>
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection
