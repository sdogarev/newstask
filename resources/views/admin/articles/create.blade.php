@extends('layouts.app')

@section('content')
<div class="container">
    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="alert alert-danger" role="alert">
                {{ $error }}
            </div>
        @endforeach
    @endif
    <form action="{{ route('admin.articles.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <label for="exampleInputEmail1" class="form-label">{{ __('articles.articles_admin_create_1') }}</label>
            <input type="text" class="form-control" name="title" id="exampleInputEmail1" aria-describedby="emailHelp">

            <label for="exampleInputEmail1" class="form-label">{{ __('articles.articles_admin_create_2') }}</label>
            <input type="text" class="form-control" name="text" id="exampleInputEmail1" aria-describedby="emailHelp">

            <label for="formFile" class="form-label">{{ __('articles.articles_admin_create_3') }}</label>
            <input class="form-control" type="file" name="image" id="formFile">

{{--            <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>--}}
            <input type="radio" class="form-check-input" name="published" value="1" id="exampleCheck1"><label class="form-check-label" for="exampleCheck1">Опубликовать</label><br>
            <input type="radio" class="form-check-input" name="published" value="0" id="exampleCheck2"><label class="form-check-label" for="exampleCheck2">Не опубликовать</label>
        <br>
{{--            <label class="form-check-label" for="exampleCheck1">Check me out</label>--}}
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
@endsection
