@extends('layouts.app')

@section('content')
    @foreach($articles as $article)
        <div class="container">
            <div class="card mb-3">
                <img src="{{ Storage::url($article->image) }}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title"><a href="{{ route('articles.show', $article->id) }}">{{ $article->title }}</a></h5>
                    <p class="card-text">{{ mb_substr($article->text, 0, 150) }}...</p>
                    <p class="card-text"><a class="btn btn-link" href="{{ route('articles.show', $article->id) }}">{{ __('articles.articles_index_link_readmore') }}</a></p>
                    <p class="card-text"><small class="text-muted">{{ __('articles.articles_index_created_at') }}: {{ $article->created_at }} | {{ __('articles.articles_index_views') }}: {{ $article->views }}</small></p>
                </div>
            </div>
        </div>
    @endforeach
    <div class="container">
        {{ $articles->links() }}
    </div>

@endsection
