@if ($errors->any())
    @foreach ($errors->all() as $error)
        <div class="alert alert-danger" role="alert">
            {{ $error }}
        </div>
    @endforeach
@endif
<form action="{{ route('comments.store') }}" method="POST" class="mb-5">
    @csrf
    <label for="exampleInputEmail1" class="form-label">Имя</label>
    <input type="text" class="form-control" name="name" id="exampleInputEmail1" aria-describedby="emailHelp">

    <label for="exampleInputEmail1" class="form-label">E-майл</label>
    <input type="email" class="form-control" name="email" id="exampleInputEmail1" aria-describedby="emailHelp">

    <label for="exampleInputEmail1" class="form-label">Текст</label>
    <textarea class="form-control" name="text" id="exampleFormControlTextarea1" rows="3"></textarea>
    <input class="form-control" type="hidden" name="article_id" value="{{ $article->id }}" id="formFile">
    <div class="g-recaptcha" data-sitekey="6LcYajgbAAAAAAL-kUpk8YK5uW_ANx6Cg1cvsVMT"></div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
