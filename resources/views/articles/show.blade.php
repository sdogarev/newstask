@extends('layouts.app')

@section('content')
    @if(session('message'))
        <div class="alert alert-success" role="alert">
            {{ session('message') }}
        </div>
    @endif
    <div class="container">
        <div class="card mb-3">
            <img src="{{ Storage::url($article->image) }}" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title">{{ $article->title }}</h5>
                <p class="card-text">{{ $article->text }}</p>
                <p class="card-text"><small class="text-muted">{{ __('articles.articles_index_created_at') }}: {{ $article->created_at }} | {{ __('articles.articles_index_views') }}: {{ $article->views }}</small></p>
            </div>
        </div>
        <hr>
        <h2>Комментарии:</h2>
        @include('articles.partial.form', ['article_id' => $article->id])
        @forelse( $comments as $comment)
            <p>{{ $comment->name }} | {{ $comment->email }}</p>
            <p>{{ $comment->text }}</p>
            <small>{{ $comment->created_at }}</small>
            <hr>
        @empty
            <p>Комментариев нет</p>
        @endforelse
        {{ $comments->links() }}
    </div>
@endsection
