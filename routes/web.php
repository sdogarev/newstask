<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\ArticleController::class, 'index'])->name('main.index');

Auth::routes();

Route::resource('articles', 'App\Http\Controllers\ArticleController')->only(['show']);
Route::resource('comments', 'App\Http\Controllers\CommentController')->only(['store']);
// Route::get('/articles/{id}', [App\Http\Controllers\ArticleController::class, 'show'])->name('main.show');
// Route::post('/comments', [App\Http\Controllers\commentcontroller::class, 'store'])->name('comment.store');

Route::group([
    'prefix' => 'admin',
    'middleware' => ['auth', 'isAdmin'],
    'as' => 'admin.'
    ], function () {

    /* Articles */
    
    Route::resource('articles', 'App\Http\Controllers\Admin\ArticleController');
    // Route::get('/articles', [App\Http\Controllers\Admin\ArticleController::class, 'index'])->name('admin.article.index');
    // Route::get('/articles/create', [App\Http\Controllers\Admin\ArticleController::class, 'create'])->name('admin.article.create');
    // Route::post('/articles/store', [App\Http\Controllers\Admin\ArticleController::class, 'store'])->name('admin.article.store');
    // Route::post('/articles/{id}', [App\Http\Controllers\Admin\ArticleController::class, 'update'])->name('admin.article.update');
    // Route::post('/articles/{id}/destroy', [App\Http\Controllers\Admin\ArticleController::class, 'destroy'])->name('admin.article.destroy');

    /* Comments */
    Route::resource('comments', 'App\Http\Controllers\Admin\CommentController');
    // Route::get('/comments', [App\Http\Controllers\Admin\CommentController::class, 'index'])->name('admin.comments.index');
    // Route::get('/comments/{id}', [App\Http\Controllers\Admin\CommentController::class, 'show'])->name('admin.comments.show');
    // Route::get('/comments/{id}/edit', [App\Http\Controllers\Admin\CommentController::class, 'edit'])->name('admin.comments.edit');
    // Route::post('/comments/{id}', [App\Http\Controllers\Admin\CommentController::class, 'update'])->name('admin.comments.update');
    // Route::post('/comments/{id}/destroy', [App\Http\Controllers\Admin\CommentController::class, 'destroy'])->name('admin.comments.destroy');

});

